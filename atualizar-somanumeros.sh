#!/bin/bash

# Atualiza pacote soma números
# Author: luancarloswd

ID_PROJETO_PACOTE="18550807"
PACOTE="SomaNumeros"
ID_PROJETO="18556373"

# Obter versão masi atualizada do projeto do pacote Nuget
for tag in $(curl --location --request GET "https://gitlab.com/api/v4/projects/${ID_PROJETO_PACOTE}/releases?private_token=${GITLAB_TOKEN}" | jq -r '.[] | @base64'); do
  tag_name=$(echo ${tag} | base64 --decode | jq -r '.tag_name')
  echo "Versão atualizada do projeto ${tag_name}"
  VERSAO_ATUALIZADA_PROJETO=$tag_name
  break
done

# Definir usuário git para utilizar no commit de atualização
git config --global user.email "$GIT_USER_EMAIL"
git config --global user.name "$GIT_USER_NAME"

MERGE_REQUEST_BRANCH="feature/atualizacao_${PACOTE}_${VERSAO_ATUALIZADA_PROJETO}" # Branch para atualização
MENSAGEM_COMMIT="Atualizando versão ${PACOTE}: ${VERSAO_ATUALIZADA_PROJETO}"     # Mensagem para o commit

# Obter projetos em que vamos atualizar
# Podemos utilizar aqui um parâmetro da api "search" caso tenha um padrão de nomenclatura
# https://docs.gitlab.com/ee/api/projects.html
PROJETO=$(curl --location --request GET "https://gitlab.com/api/v4/projects/${ID_PROJETO}?private_token=${GITLAB_TOKEN}")
_jq() {
  echo "${PROJETO}" | jq -r "${1}"
}

API_PATH=$(_jq '.path')
URL_REPO=$(_jq '.http_url_to_repo')
URL_REPO=${URL_REPO#"https://"} # Removemos o 'https://' pois vamos concatenar com o usuário e token para clonar o projeto
echo "${API_PATH}"

# Clonar projeto da origem
git clone https://${GITLAB_USER}:${GITLAB_TOKEN}@${URL_REPO}
cd "${API_PATH}" || exit 0

# Iterar sobre cada arquivo csproj
for csproj in $(find . -print | grep -i '.*[.]csproj'); do
  # Alterar a versão do pacote
  sed -i '/\'${PACOTE}'/c\    <PackageReference Include="'${PACOTE}'" Version="'${VERSAO_ATUALIZADA_PROJETO}'" />' ${csproj}

  # Verificar se houve alterações na branch
  if [ -n "$(git status --porcelain)" ]; then
    git checkout -b "${MERGE_REQUEST_BRANCH}" # Criar branch para atualização
    git add *.csproj                          # Adicionar arquivos .csproj modificados
    git commit -m "${MENSAGEM_COMMIT}"        # Fazer um commit
    git push -u origin HEAD                   # Enviar a branch criada para origem

    # Realizar Merge Request
    curl -X POST \
      https://gitlab.com/api/v4/projects/${ID_PROJETO}/merge_requests \
      -H "cache-control: no-cache" \
      -H "content-type: multipart/form-data" \
      -H "private-token: ${GITLAB_TOKEN}" \
      -F source_branch="${MERGE_REQUEST_BRANCH}" \
      -F target_branch="master" \
      -F "title=${MENSAGEM_COMMIT}" \
      -F remove_source_branch=true
  else
    echo "Projeto não precisou de atualização"
  fi
done
